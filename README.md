# BERT Subsumption

The folder [Intra_Ontology](https://gitlab.com/chen00217/bert_subsumption/-/tree/master/Intra_Ontology) includes the implementation of BERTSubs -- a class subsumption prediction method with BERT-based contextual class embeddings, 
as well as its codes for experiments involved in
the KR'2022 conference submission "Contextual Semantic Embeddings for Ontology Subsumption Prediction".

Requirements: sklearn 0.24.2, transformers 4.10.2, Owlready2 0.34, torch 1.9.0, pandas 1.3.3

Note we are packaging the current BERTSubs implementation as a more usable tool, 
and are to integrate it with other ontology curation tools such as [BERTMap](https://github.com/KRR-Oxford/BERTMap) and [OWL2Vec*](https://github.com/KRR-Oxford/OWL2Vec-Star) as one ontology curation package named [DeepOnto](https://github.com/KRR-Oxford/DeepOnto).