import argparse
import sys

from owlready2 import *
from ontology import MyOntology

parser = argparse.ArgumentParser()
parser.add_argument('--onto_file', type=str, default='../../ontologies/foodon-merged.0.4.8.owl',
                    help='valid/test subsumptions are masked from the triples')
parser.add_argument('--out_dir', type=str, default='../foodon/foodon_train_kg')
parser.add_argument('--subsumption_type', type=str, default='named class', help='named class, or restriction')
parser.add_argument('--restriction_label_property_id', type=int, default=3,
                    help='this id should be added to property2id.txt')
parser.add_argument('--label_property', type=list, default=["http://www.w3.org/2000/01/rdf-schema#label",
                                                         #  "http://www.geneontology.org/formats/oboInOwl#hasSynonym",
                                                            "http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"])
FLAGS, unparsed = parser.parse_known_args()


if not os.path.exists(FLAGS.out_dir):
    print('No graph found')
    sys.exit(0)

onto = MyOntology(onto_file=FLAGS.onto_file, label_property=FLAGS.label_property)

entity_id = dict()
with open(os.path.join(FLAGS.out_dir, 'entity2id.txt')) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        entity_id[tmp[0]] = int(tmp[1])
relation_id = dict()
with open(os.path.join(FLAGS.out_dir, 'relation2id.txt')) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        relation_id[tmp[0]] = int(tmp[1])
triple2id = list()
with open(os.path.join(FLAGS.out_dir, 'train2id.txt')) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        triple2id.append([int(tmp[0]), int(tmp[1]), int(tmp[2])])
with open(os.path.join(FLAGS.out_dir, 'valid2id.txt')) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        triple2id.append([int(tmp[0]), int(tmp[1]), int(tmp[2])])
with open(os.path.join(FLAGS.out_dir, 'test2id.txt')) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        triple2id.append([int(tmp[0]), int(tmp[1]), int(tmp[2])])

sub_index = dict()
for t in triple2id:
    if t[0] in sub_index:
        sub_index[t[0]].add('%d,%d' % (t[1], t[2]))
    else:
        sub_index[t[0]] = {'%d,%d' % (t[1], t[2])}
restrictionIDs = set()
for t in triple2id:
    sub, pred, obj = t[0], t[2], t[1]
    if obj == entity_id['http://www.w3.org/2002/07/owl#Restriction'] and \
            pred == relation_id['http://www.w3.org/1999/02/22-rdf-syntax-ns#type']:
        restrictionIDs.add(sub)


def restriction2id(r):
    rid = None
    a1 = '%d,%d' % (entity_id[r.property.iri], relation_id['http://www.w3.org/2002/07/owl#onProperty'])
    a2 = '%d,%d' % (entity_id[r.value.iri], relation_id['http://www.w3.org/2002/07/owl#someValuesFrom'])
    for rID in restrictionIDs:
        if a1 in sub_index[rID] and a2 in sub_index[rID]:
            rid = rID
            break
    return rid


annotations = list()
with open(os.path.join(FLAGS.out_dir, 'annotations.txt')) as f:
    for line in f.readlines()[1:]:
        annotations.append(line.strip())


print('existing annotations: %d' % len(annotations))

if FLAGS.subsumption_type == 'restriction':
    for restrictionName in onto.restrictionsName_label:
        labels = onto.restrictionsName_label[restrictionName]
        restriction_id = entity_id[restrictionName]
        if len(labels) > 0:
            ann = '%d\t%d\t%s' % (restriction_id, FLAGS.restriction_label_property_id, labels[0])
            annotations.append(ann)

elif FLAGS.subsumption_type == 'named class':
    for restriction in onto.restrictions:
        restrictionName = str(restriction)
        labels = onto.restrictionsName_label[restrictionName]
        restriction_id = restriction2id(r=restriction)
        if restriction_id is not None:
            if len(labels) > 0:
                ann = '%d\t%d\t%s' % (restriction_id, FLAGS.restriction_label_property_id, labels[0])
                annotations.append(ann)

else:
    print('Unknown subsumption type: %s' % FLAGS.subsumption_type)
    sys.exit(0)

with open(os.path.join(FLAGS.out_dir, 'annotations_restriction.txt'), 'w') as f:
    f.write('%d\n' % len(annotations))
    for ann in annotations:
        f.write('%s\n' % ann)

print('new annotations: %d' % len(annotations))
