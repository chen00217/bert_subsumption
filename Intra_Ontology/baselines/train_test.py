import argparse
import gensim
from ontology import MyOntology

import numpy as np
from nltk import word_tokenize
from owlready2 import *
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression

parser = argparse.ArgumentParser()
parser.add_argument('--subsumption_type', type=str, default='restriction',
                    help='named_class, or restriction; for restriction, vector type is set to word only')
parser.add_argument('--train_subsumption_file', type=str, default='../GO/train_subsumptions_r.csv')
parser.add_argument('--test_subsumption_file', type=str, default='../GO/test_subsumptions_r.csv')
parser.add_argument('--valid_subsumption_file', type=str, default='../GO/valid_subsumptions_r.csv')
parser.add_argument('--train_onto_file', type=str, default='../../ontologies/go.origin.07012022.owl',
                    help='the ontology is only used to get the labels; no test/valid subsumptions will be used')
parser.add_argument('--label_property', type=str, default='http://www.w3.org/2000/01/rdf-schema#label')
parser.add_argument('--train_pos_dup', type=int, default=2)
parser.add_argument('--train_neg_dup', type=int, default=2)
parser.add_argument('--vector_type', type=str, default='word', help='iri, iri_word, word')
parser.add_argument('--classifier', type=str, default='rf', help='rf,mlp,lr')
parser.add_argument('--word2vec_embedding_file', type=str,
                    default='../GO/go.w2v.model',
                    help='/Users/jiahen/Data/w2v_model/enwiki_model/word2vec_gensim')
FLAGS, unparsed = parser.parse_known_args()


def pre_process_label(input_label):
    text = ' '.join([re.sub(r'https?:\/\/.*[\r\n]*', '', w, flags=re.MULTILINE) for w in input_label.lower().split()])
    tokens = word_tokenize(text)
    return [token.lower() for token in tokens if token.isalpha()]


start_time = datetime.datetime.now()

onto = MyOntology(onto_file=FLAGS.train_onto_file, label_property=[FLAGS.label_property])
w2v = gensim.models.Word2Vec.load(FLAGS.word2vec_embedding_file)
iri_embedding = dict()
for c in onto.named_classes:
    word_vector = np.zeros(w2v.vector_size)
    n = 0
    if c.iri in onto.iri_label:
        for lab in onto.iri_label[c.iri]:
            words = pre_process_label(input_label=lab)
            for word in words:
                if word in w2v.wv.index_to_key:
                    word_vector += w2v.wv.get_vector(word)
                    n += 1
    word_vector = word_vector / n if n > 0 else word_vector

    if FLAGS.vector_type == 'iri':
        iri_embedding[c.iri] = w2v.wv.get_vector(c.iri) if c.iri in w2v.wv.index_to_key else np.zeros(w2v.vector_size)
    elif FLAGS.vector_type == 'word':
        iri_embedding[c.iri] = word_vector
    elif FLAGS.vector_type == 'iri_word':
        iri_vector = w2v.wv.get_vector(c.iri) if c.iri in w2v.wv.index_to_key else np.zeros(w2v.vector_size)
        iri_embedding[c.iri] = np.concatenate((iri_vector, word_vector))
    else:
        print('Unknown vector type')
        sys.exit(0)

for restrictionName in onto.restrictionsName_label:
    n = 0
    word_vector = np.zeros(w2v.vector_size)
    for lab in onto.restrictionsName_label[restrictionName]:
        words = pre_process_label(input_label=lab)
        for word in words:
            if word in w2v.wv.index_to_key:
                word_vector += w2v.wv.get_vector(word)
                n += 1
    word_vector = word_vector / n if n > 0 else word_vector
    iri_embedding[restrictionName] = word_vector



read_subsumptions = lambda file_name: [line.strip().split(',') for line in open(file_name).readlines()]
train_subsumptions = read_subsumptions(FLAGS.train_subsumption_file)

neg_subsumptions = list()
for subs in train_subsumptions:
    c1 = subs[0]
    for _ in range(FLAGS.train_neg_dup):
        neg_c = onto.get_negative_sample(subclass_str=c1, subsumption_type=FLAGS.subsumption_type)
        neg_subsumptions.append([c1, neg_c])
pos_subsumptions = FLAGS.train_pos_dup * train_subsumptions
print('Positive train subsumptions: %d' % len(pos_subsumptions))
print('Negative train subsumptions: %d' % len(neg_subsumptions))

subsumption_vector = lambda subsumption: np.concatenate((iri_embedding[subsumption[0]], iri_embedding[subsumption[1]]))
pos_X = [subsumption_vector(s) for s in pos_subsumptions]
pos_y = np.ones((len(pos_X)))
pos_X = np.array(pos_X)
neg_X = [subsumption_vector(s) for s in neg_subsumptions]
neg_y = np.zeros((len(neg_X)))
neg_X = np.array(neg_X)
X, y = np.concatenate((pos_X, neg_X)), np.concatenate((pos_y, neg_y))
X, y = shuffle(X, y, random_state=0)

if FLAGS.classifier == 'rf':
    model = RandomForestClassifier(n_estimators=50)
elif FLAGS.classifier == 'mlp':
    model = MLPClassifier(max_iter=1000, hidden_layer_sizes=200)
else:
    model = LogisticRegression(random_state=0)
model.fit(X, y)

end_time = datetime.datetime.now()
print('data pre-processing and training cost %.1f minutes' % ((end_time - start_time).seconds / 60))


def evaluate(target_subsumptions, test_type='test'):
    MRR_sum, hits1_sum, hits5_sum, hits10_sum = 0, 0, 0, 0
    MRR, Hits1, Hits5, Hits10 = 0, 0, 0, 0
    for k, test in enumerate(target_subsumptions):
        subcls, gt = test[0], test[1]
        candidates = test[1:]
        candidate_subsumptions = [[subcls, c] for c in candidates]

        V = np.array([subsumption_vector(candidate_subsumption) for candidate_subsumption in candidate_subsumptions])
        P = model.predict_proba(V)[:, 1]
        sorted_indexes = np.argsort(P)[::-1]
        sorted_classes = list()
        for j in sorted_indexes:
            sorted_classes.append(candidates[j])
        rank = sorted_classes.index(gt) + 1

        MRR_sum += 1.0 / rank
        hits1_sum += 1 if gt in sorted_classes[:1] else 0
        hits5_sum += 1 if gt in sorted_classes[:5] else 0
        hits10_sum += 1 if gt in sorted_classes[:10] else 0
        num = k + 1
        MRR, Hits1, Hits5, Hits10 = MRR_sum / num, hits1_sum / num, hits5_sum / num, hits10_sum / num
        if num % 500 == 0:
            print('\n%d tested, MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (num, MRR, Hits1, Hits5, Hits10))
    print('\n[%s], MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (test_type, MRR, Hits1, Hits5, Hits10))


valid_subsumptions = read_subsumptions(FLAGS.valid_subsumption_file)
evaluate(target_subsumptions=valid_subsumptions, test_type='valid')

test_subsumptions = read_subsumptions(FLAGS.test_subsumption_file)
evaluate(target_subsumptions=test_subsumptions, test_type='test')
