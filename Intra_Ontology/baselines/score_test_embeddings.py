import argparse
import json
import sys

import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--test_subsumption_file', type=str, default='../GO/test_subsumptions_r.csv')
parser.add_argument('--valid_subsumption_file', type=str, default='../GO/valid_subsumptions_r.csv')
parser.add_argument('--subsumption_type', type=str, default='restriction', help='named class, or restriction')
parser.add_argument('--embedding_file', type=str, default='../GO/go_train_r_kg/embedding.vec.json')
parser.add_argument('--entity_file', type=str, default='../GO/go_train_r_kg/entity2id.txt')
parser.add_argument('--relation_file', type=str, default='../GO/go_train_r_kg/relation2id.txt')
parser.add_argument('--kge_model', type=str, default='distmult', help='transE, transR, distmult')
FLAGS, unparsed = parser.parse_known_args()


embed_f = open(FLAGS.embedding_file, 'r')
embeddings = json.loads(embed_f.read())
ent_embeddings = embeddings['ent_embeddings']
rel_embeddings = embeddings['rel_embeddings']
embedsize = len(ent_embeddings[0])
entity_id = dict()
with open(FLAGS.entity_file) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        entity_id[tmp[0]] = int(tmp[1])
relation_id = dict()
with open(FLAGS.relation_file) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        relation_id[tmp[0]] = int(tmp[1])
rid = relation_id['http://www.w3.org/2000/01/rdf-schema#subClassOf']
subClassOf_v = rel_embeddings[rid]


def subsumption_score(subsumptions):
    P = np.zeros(len(subsumptions))
    for i, subsumption in enumerate(subsumptions):

        if subsumption[0] not in entity_id:
            print('%s not in entity_id' % subsumption[0])
            sub_v = np.zeros(embedsize)
        else:
            sub_id = entity_id[subsumption[0]]
            sub_v = np.array(ent_embeddings[sub_id])
        if subsumption[1] not in entity_id:
            print('%s not in entity_id' % subsumption[1])
            sup_v = np.zeros(embedsize)
        else:
            sup_id = entity_id[subsumption[1]]
            sup_v = np.array(ent_embeddings[sup_id])

        if FLAGS.kge_model == 'transE':
            score = np.reciprocal(np.mean(abs(sub_v + subClassOf_v - sup_v)))
        elif FLAGS.kge_model == 'distmult':
            score = np.sum(sub_v * subClassOf_v * sup_v)
        else:
            print('%s not implemented' % FLAGS.kge_model)
            sys.exit(0)

        P[i] = score
    return P



def evaluate(target_subsumptions, test_type='test'):
    MRR_sum, hits1_sum, hits5_sum, hits10_sum = 0, 0, 0, 0
    MRR, Hits1, Hits5, Hits10 = 0, 0, 0, 0
    for k, test in enumerate(target_subsumptions):
        subcls, gt = test[0], test[1]
        candidates = test[1:]
        can_subsumptions = [[subcls, can] for can in candidates]
        P = subsumption_score(subsumptions=can_subsumptions)
        sorted_indexes = np.argsort(P)[::-1]
        sorted_classes = list()
        for j in sorted_indexes:
            sorted_classes.append(candidates[j])
        rank = sorted_classes.index(gt) + 1

        MRR_sum += 1.0 / rank
        hits1_sum += 1 if gt in sorted_classes[:1] else 0
        hits5_sum += 1 if gt in sorted_classes[:5] else 0
        hits10_sum += 1 if gt in sorted_classes[:10] else 0
        num = k + 1
        MRR, Hits1, Hits5, Hits10 = MRR_sum / num, hits1_sum / num, hits5_sum / num, hits10_sum / num
        if num % 500 == 0:
            print('\n%d tested, MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (
            num, MRR, Hits1, Hits5, Hits10))
    print('\n[%s], MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (test_type, MRR, Hits1, Hits5, Hits10))


read_subsumptions = lambda file_name: [ll.strip().split(',') for ll in open(file_name).readlines()]
valid_subsumptions = read_subsumptions(FLAGS.valid_subsumption_file)
evaluate(target_subsumptions=valid_subsumptions, test_type='valid')
test_subsumptions = read_subsumptions(FLAGS.test_subsumption_file)
evaluate(target_subsumptions=test_subsumptions, test_type='test')
