# generate the ontology for training (i.e., the original ontology - test and valid subsumptions)
# consider subsumption between named classes, and subsumptions between named classes and restrictions

### Important: when a restriction subsumer is deleted from the parents of a subsume,
###            this restriction is fully deleted from the ontology if it does not appear as the subsumer of other classes.

import argparse
from owlready2 import *

parser = argparse.ArgumentParser()
parser.add_argument('--onto_file', type=str, default='../../ontologies/go.origin.07012022.owl')
parser.add_argument('--valid_subsumption_file', type=str, default='../GO/valid_subsumptions_r.csv')
parser.add_argument('--test_subsumption_file', type=str, default='../GO/test_subsumptions_r.csv')
parser.add_argument('--train_onto_file', type=str, default='../GO/go_train_r.owl')
FLAGS, unparsed = parser.parse_known_args()

read_subsumptions = lambda file_name: [line.strip().split(',') for line in open(file_name).readlines()]
test_subsumptions = read_subsumptions(FLAGS.test_subsumption_file)
valid_subsumptions = read_subsumptions(FLAGS.valid_subsumption_file)

onto = get_ontology(FLAGS.onto_file).load()

name_restriction = dict()
for c in onto.classes():
    for parent in c.is_a:
        if type(parent) == class_construct.Restriction:
            name_restriction[str(parent)] = parent

for subsumptions in valid_subsumptions + test_subsumptions:
    s1, s2 = subsumptions[0], subsumptions[1]
    subc = IRIS[s1]
    supc = name_restriction[s2] if s2 in name_restriction else IRIS[s2]
    if supc in subc.is_a:
        subc.is_a.remove(supc)
    else:
        print('wrong subsumption: %s, %s' % (subc, supc))

onto.save(file=FLAGS.train_onto_file, format='rdfxml')
print('train ontology saved!')
