import argparse
import json
import sys
import pickle

from ontology import MyOntology
import numpy as np
from owlready2 import *
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression

parser = argparse.ArgumentParser()
parser.add_argument('--train_subsumption_file', type=str, default='../FoodOn/train_subsumptions.csv')
parser.add_argument('--test_subsumption_file', type=str, default='../FoodOn/test_subsumptions.csv')
parser.add_argument('--valid_subsumption_file', type=str, default='../FoodOn/valid_subsumptions.csv')
parser.add_argument('--train_onto_file', type=str, default='../FoodOn/foodon_train.owl')
parser.add_argument('--subsumption_type', type=str, default='named class', help='named class, or restriction')
parser.add_argument('--train_pos_dup', type=int, default=2)
parser.add_argument('--train_neg_dup', type=int, default=2)
parser.add_argument("--embedding_file", type=str, default='../FoodOn/foodon_train_kg/saved_embeddings_text_w2v.pkl')
parser.add_argument("--entity_file", type=str, default='../FoodOn/foodon_train_kg/entity2id.txt')
parser.add_argument('--classifier', type=str, default='rf', help='rf,mlp,lr')
parser.add_argument('--label_property', type=list, default=["http://www.w3.org/2000/01/rdf-schema#label",
                                                            "http://www.geneontology.org/formats/oboInOwl#hasSynonym",
                                                            "http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"])
FLAGS, unparsed = parser.parse_known_args()

start_time = datetime.datetime.now()

onto = MyOntology(onto_file=FLAGS.train_onto_file, label_property=FLAGS.label_property)

if FLAGS.embedding_file.endswith('.json'):
    embed_f = open(FLAGS.embedding_file, 'r')
    embeddings = json.loads(embed_f.read())
    ent_embeddings = embeddings['ent_embeddings']
elif FLAGS.embedding_file.endswith('.npy'):
    ent_embeddings = np.load(FLAGS.embedding_file)
elif FLAGS.embedding_file.endswith('.pkl'):
    embed_f = open(FLAGS.embedding_file, 'rb')
    embeddings = pickle.load(embed_f)
    ent_embeddings = embeddings['ent_embeddings']
else:
    print('Unknown embedding file format')
    sys.exit(0)

embedsize = len(ent_embeddings[0])
entity_id = dict()
with open(FLAGS.entity_file) as f:
    for line in f.readlines()[1:]:
        tmp = line.strip().split('\t')
        entity_id[tmp[0]] = int(tmp[1])


read_subsumptions = lambda file_name: [ll.strip().split(',') for ll in open(file_name).readlines()]
train_subsumptions = read_subsumptions(FLAGS.train_subsumption_file)

neg_subsumptions = list()
for subs in train_subsumptions:
    c1 = subs[0]
    for _ in range(FLAGS.train_neg_dup):
        neg_c = onto.get_negative_sample(subclass_str=c1, subsumption_type=FLAGS.subsumption_type)
        neg_subsumptions.append([c1, neg_c])
pos_subsumptions = FLAGS.train_pos_dup * train_subsumptions
print('Positive train subsumptions: %d' % len(pos_subsumptions))
print('Negative train subsumptions: %d' % len(neg_subsumptions))


def subsumption_vector(subsumption):
    if subsumption[0] not in entity_id:
        print('%s not in entity_id' % subsumption[0])
        sub_v = np.zeros(embedsize)
    else:
        sub_id = entity_id[subsumption[0]]
        if type(ent_embeddings) == dict and sub_id not in ent_embeddings:
            sub_v = np.zeros(embedsize)
            print(sub_id)
        else:
            sub_v = np.array(ent_embeddings[sub_id])
    if subsumption[1] not in entity_id:
        print('%s not in entity_id' % subsumption[1])
        sup_v = np.zeros(embedsize)
    else:
        sup_id = entity_id[subsumption[1]]
        if type(ent_embeddings) == dict and sup_id not in ent_embeddings:
            sup_v = np.zeros(embedsize)
            print(sup_id)
        else:
            sup_v = np.array(ent_embeddings[sup_id])
    return np.concatenate((sub_v, sup_v))


pos_X = [subsumption_vector(subsumption=s) for s in pos_subsumptions]
pos_y = np.ones((len(pos_X)))
pos_X = np.array(pos_X)
neg_X = [subsumption_vector(subsumption=s) for s in neg_subsumptions]
neg_y = np.zeros((len(neg_X)))
neg_X = np.array(neg_X)
X, y = np.concatenate((pos_X, neg_X)), np.concatenate((pos_y, neg_y))
X, y = shuffle(X, y, random_state=0)

if FLAGS.classifier == 'rf':
    model = RandomForestClassifier(n_estimators=100)
elif FLAGS.classifier == 'mlp':
    model = MLPClassifier(max_iter=1000, hidden_layer_sizes=200)
else:
    model = LogisticRegression(random_state=0)
model.fit(X, y)

end_time = datetime.datetime.now()
print('data pre-processing and training cost %.1f minutes' % ((end_time - start_time).seconds / 60))


def evaluate(target_subsumptions, test_type='test'):
    MRR_sum, hits1_sum, hits5_sum, hits10_sum = 0, 0, 0, 0
    MRR, Hits1, Hits5, Hits10 = 0, 0, 0, 0
    for k, test in enumerate(target_subsumptions):
        subcls, gt = test[0], test[1]
        candidates = test[1:]
        can_subsumptions = [[subcls, can] for can in candidates]

        V = np.array([subsumption_vector(subsumption=can_subsumption) for can_subsumption in can_subsumptions])
        P = model.predict_proba(V)[:, 1]
        sorted_indexes = np.argsort(P)[::-1]
        sorted_classes = list()
        for j in sorted_indexes:
            sorted_classes.append(candidates[j])
        rank = sorted_classes.index(gt) + 1

        MRR_sum += 1.0 / rank
        hits1_sum += 1 if gt in sorted_classes[:1] else 0
        hits5_sum += 1 if gt in sorted_classes[:5] else 0
        hits10_sum += 1 if gt in sorted_classes[:10] else 0
        num = k + 1
        MRR, Hits1, Hits5, Hits10 = MRR_sum / num, hits1_sum / num, hits5_sum / num, hits10_sum / num
        if num % 500 == 0:
            print('\n%d tested, MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (
            num, MRR, Hits1, Hits5, Hits10))
    print('\n[%s], MRR: %.3f, Hits@1: %.3f, Hits@5: %.3f, Hits@10: %.3f\n' % (test_type, MRR, Hits1, Hits5, Hits10))


valid_subsumptions = read_subsumptions(FLAGS.valid_subsumption_file)
evaluate(target_subsumptions=valid_subsumptions, test_type='valid')

test_subsumptions = read_subsumptions(FLAGS.test_subsumption_file)
evaluate(target_subsumptions=test_subsumptions, test_type='test')
