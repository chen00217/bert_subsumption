import sys

import rdflib
import argparse
from owlready2 import *
from ontology import MyOntology

parser = argparse.ArgumentParser()
parser.add_argument('--onto_file', type=str, default='../../ontologies/go.origin.07012022.owl',
                    help='valid/test subsumptions are masked from the triples')
parser.add_argument('--out_dir', type=str, default='../GO/go_train_r_kg')
parser.add_argument('--train_subsumption_file', type=str, default='../GO/train_subsumptions_r.csv')
parser.add_argument('--test_subsumption_file', type=str, default='../GO/test_subsumptions_r.csv')
parser.add_argument('--valid_subsumption_file', type=str, default='../GO/valid_subsumptions_r.csv')
parser.add_argument('--subsumption_type', type=str, default='restriction', help='named class, or restriction')
parser.add_argument('--label_property', type=list, default=["http://www.w3.org/2000/01/rdf-schema#label",
                                                         #   "http://www.geneontology.org/formats/oboInOwl#hasSynonym",
                                                            "http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"])
FLAGS, unparsed = parser.parse_known_args()

if not os.path.exists(FLAGS.out_dir):
    os.mkdir(FLAGS.out_dir)

onto = MyOntology(onto_file=FLAGS.onto_file, label_property=FLAGS.label_property)

g = rdflib.Graph()
g.parse(FLAGS.onto_file)

ann_properties = set()
for (s, p, o) in g:
    p = str(p)
    p = p.strip().replace('\t', ' ').replace('\n', '')
    if type(IRIS[p]) == AnnotationPropertyClass:
        ann_properties.add(p)

entities, relations, triples = set(), set(), list()
ann_triples = list()
for (s, p, o) in g:
    s, p, o = str(s), str(p), str(o)
    s = s.strip().replace('\t', ' ').replace('\n', '')
    p = p.strip().replace('\t', ' ').replace('\n', '')
    o = o.strip().replace('\t', ' ').replace('\n', '')
    if s == '' or p == '' or o == '':
        continue
    if p in FLAGS.label_property:
        ann_triples.append([s, p, o])
    if s in ann_properties or p in ann_properties or o in ann_properties or \
            p == 'http://www.w3.org/2002/07/owl#annotatedTarget' or \
            p == 'http://www.w3.org/2002/07/owl#annotatedSource':
        continue
    triples.append([s, p, o])
    entities.add(s)
    entities.add(o)
    relations.add(p)
entities, relations = list(entities), list(relations)
print('entities: %d, relations: %d, triples: %d' % (len(entities), len(relations), len(triples)))

entity_id, relation_id = dict(), dict()
for i, entity in enumerate(entities):
    entity_id[entity] = i
for i, relation in enumerate(relations):
    relation_id[relation] = i

triple2id = list()
for i, triple in enumerate(triples):
    sub_id = entity_id[triple[0]]
    rel_id = relation_id[triple[1]]
    obj_id = entity_id[triple[2]]
    triple2id.append([sub_id, obj_id, rel_id])

with open(os.path.join(FLAGS.out_dir, 'property2id.txt'), 'w') as f:
    f.write('%d\n' % len(FLAGS.label_property))
    for i, p in enumerate(FLAGS.label_property):
        f.write('%s\t%d\n' % (p, i))
ann_triples2 = list()
for t in ann_triples:
    if t[0] in entity_id:
        ann_triples2.append([entity_id[t[0]], FLAGS.label_property.index(t[1]), t[2]])
with open(os.path.join(FLAGS.out_dir, 'annotations.txt'), 'w') as f:
    f.write('%d\n' % len(ann_triples2))
    for i, triple in enumerate(ann_triples2):
        f.write('%d\t%d\t%s\n' % (triple[0], triple[1], triple[2]))

read_subsumptions = lambda file_name: [ll.strip().split(',') for ll in open(file_name).readlines()]
valid_subsumptions = read_subsumptions(FLAGS.valid_subsumption_file)
test_subsumptions = read_subsumptions(FLAGS.test_subsumption_file)
train_subsumptions = read_subsumptions(FLAGS.train_subsumption_file)

if FLAGS.subsumption_type == 'restriction':
    sub_index = dict()
    for t in triple2id:
        if t[0] in sub_index:
            sub_index[t[0]].add('%d,%d' % (t[1], t[2]))
        else:
            sub_index[t[0]] = {'%d,%d' % (t[1], t[2])}
    restrictionIDs = set()
    for t in triple2id:
        sub, pred, obj = t[0], t[2], t[1]
        if obj == entity_id['http://www.w3.org/2002/07/owl#Restriction'] and \
                pred == relation_id['http://www.w3.org/1999/02/22-rdf-syntax-ns#type']:
            restrictionIDs.add(sub)
    entityID_restriction = dict()
    mask2id = list()


    def address_subsumptions(subsumptions, subs_type='train'):
        for subsum in subsumptions:
            subsumer_id = None
            subsumer = onto.name_restrictions[subsum[1]]
            a1 = '%d,%d' % (entity_id[subsumer.property.iri], relation_id['http://www.w3.org/2002/07/owl#onProperty'])
            a2 = '%d,%d' % (entity_id[subsumer.value.iri], relation_id['http://www.w3.org/2002/07/owl#someValuesFrom'])
            for rID in restrictionIDs:
                if a1 in sub_index[rID] and a2 in sub_index[rID]:
                    subsumer_id = rID
                    break
            if subsumer_id is None:
                print('[%s] %s has NO id' % (subs_type, subsum[1]))
                sys.exit(0)
            entityID_restriction[subsumer_id] = subsum[1]

            if subs_type == 'valid' or subs_type == 'test':
                mask2id.append('%d,%d' % (entity_id[subsum[0]], subsumer_id))


    address_subsumptions(subsumptions=train_subsumptions, subs_type='train')
    address_subsumptions(subsumptions=valid_subsumptions, subs_type='valid')
    address_subsumptions(subsumptions=test_subsumptions, subs_type='test')

    entity_id2 = dict()
    for ent in entity_id:
        entityID = entity_id[ent]
        if entityID in entityID_restriction:
            restriction = entityID_restriction[entityID]
            entity_id2[restriction] = entityID
        else:
            entity_id2[ent] = entityID
    entity_id = entity_id2

else:
    mask2id = list()
    for subsumption in valid_subsumptions + test_subsumptions:
        mask2id.append('%d,%d' % (entity_id[subsumption[0]], entity_id[subsumption[1]]))


subClassOf_id = relation_id['http://www.w3.org/2000/01/rdf-schema#subClassOf']
valid2id, test2id = list(), list()
for subs in valid_subsumptions:
    valid2id.append([entity_id[subs[0]], entity_id[subs[1]], subClassOf_id])
for subs in test_subsumptions:
    test2id.append([entity_id[subs[0]], entity_id[subs[1]], subClassOf_id])


with open(os.path.join(FLAGS.out_dir, 'test2id.txt'), 'w') as f:
    f.write('%d\n' % len(test2id))
    for i, triple in enumerate(test2id):
        f.write('%d\t%d\t%d\n' % (triple[0], triple[1], triple[2]))
with open(os.path.join(FLAGS.out_dir, 'valid2id.txt'), 'w') as f:
    f.write('%d\n' % len(valid2id))
    for i, triple in enumerate(valid2id):
        f.write('%d\t%d\t%d\n' % (triple[0], triple[1], triple[2]))

with open(os.path.join(FLAGS.out_dir, 'entity2id.txt'), 'w') as f:
    f.write('%d\n' % len(entity_id))
    for entity in entity_id:
        f.write('%s\t%d\n' % (entity, entity_id[entity]))
with open(os.path.join(FLAGS.out_dir, 'relation2id.txt'), 'w') as f:
    f.write('%d\n' % len(relation_id))
    for relation in relation_id:
        f.write('%s\t%d\n' % (relation, relation_id[relation]))
with open(os.path.join(FLAGS.out_dir, 'train2id.txt'), 'w') as f:
    f.write('%d\n' % len(triple2id))
    for t in triple2id:
        if ('%d,%d' % (t[0], t[1])) not in mask2id:
            f.write('%d\t%d\t%d\n' % (t[0], t[1], t[2]))
