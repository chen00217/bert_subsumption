import random
import argparse
from owlready2 import *
from ontology import MyOntology

'''
    partition the declared subsumptions into train (80%), valid (5%) and test (15%)
    when restriction == False:
        a test sample is composed of two named classes: a subclass, a superclass (GT), 
        and at most 50 false superclasses extracted from the GT's neighbourhood
    when restriction == True:
        a sample is composed of a named class (subclass), an existential restriction (superclass GT), 
        and at most 50 false restrictions extracted with relevant classes and relations.
'''

parser = argparse.ArgumentParser()
parser.add_argument('--onto_file', type=str, default='../ontologies/foodon-merged.0.4.8.owl')
parser.add_argument('--valid_ratio', type=float, default=0.05)
parser.add_argument('--test_ratio', type=float, default=0.15)
parser.add_argument('--test_max_neg_size', type=int, default=40)
parser.add_argument('--test_neg_random_restriction_size', type=int, default=10)
parser.add_argument('--max_depth', type=int, default=3)
parser.add_argument('--max_width', type=int, default=8)
parser.add_argument('--restriction', type=bool, default=True)
parser.add_argument('--train_file', type=str, default='./FoodOn/train_subsumptions_r.csv')
parser.add_argument('--valid_file', type=str, default='./FoodOn/valid_subsumptions_r.csv')
parser.add_argument('--test_file', type=str, default='./FoodOn/test_subsumptions_r.csv')
FLAGS, unparsed = parser.parse_known_args()

onto = MyOntology(onto_file=FLAGS.onto_file,
                  label_property=["http://www.w3.org/2000/01/rdf-schema#label",
                                  "http://www.geneontology.org/formats/oboInOwl#hasSynonym",
                                  "http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"])
named_classes = onto.get_named_classes()
print('%d named classes' % len(named_classes))
if FLAGS.restriction:
    declared_subsumptions = onto.get_declared_restriction_subsumption()
    print('%d existential restrictions' % len(onto.restrictions))
else:
    declared_subsumptions = onto.get_declared_named_class_subsumption()
print('%d declared subsumptions' % len(declared_subsumptions))

valid_size = int(len(declared_subsumptions) * FLAGS.valid_ratio)
test_size = int(len(declared_subsumptions) * FLAGS.test_ratio)
valid_subsumptions = declared_subsumptions[0:valid_size]
test_subsumptions = declared_subsumptions[valid_size:(valid_size + test_size)]
train_subsumptions = declared_subsumptions[(valid_size + test_size):]
print('train subsumptions: %d' % len(train_subsumptions))
print('valid subsumptions: %d' % len(valid_subsumptions))
print('test subsumptions: %d' % len(test_subsumptions))


def get_test_neg_candidates(subclass, gt):
    all_nebs, seeds = set(), [gt]
    depth = 1
    while depth <= FLAGS.max_depth:
        new_seeds = set()
        for seed in seeds:
            nebs = MyOntology.get_one_hop_neighbours(c=seed)
            new_seeds = new_seeds.union(nebs)
            all_nebs = all_nebs.union(nebs)
        depth += 1
        seeds = random.sample(new_seeds, FLAGS.max_width) if len(new_seeds) > FLAGS.max_width else new_seeds
    all_nebs = all_nebs - subclass.ancestors() - {subclass}
    if len(all_nebs) > FLAGS.test_max_neg_size:
        return random.sample(all_nebs, FLAGS.test_max_neg_size)
    else:
        return list(all_nebs)


def context_candidate(output_file, subsumptions):
    with open(output_file, 'w') as ff:
        size_sum = 0
        size_num = dict()
        m = 0
        for subcls, supcls in subsumptions:
            neg_candidates = get_test_neg_candidates(subclass=subcls, gt=supcls)
            size = len(neg_candidates)
            size_sum += size
            size_num[size] = size_num[size] + 1 if size in size_num else 1
            if size > 0:
                s = ','.join([c.iri for c in neg_candidates])
                ff.write('%s,%s,%s\n' % (subcls.iri, supcls.iri, s))
                m += 1
        print('\t The distribution of negative candidate size:')
        for size in range(FLAGS.test_max_neg_size + 1):
            if size in size_num:
                print('\t size: %d, num: %d' % (size, size_num[size]))
            else:
                print('\t size: %d, num: 0' % size)
        print('\t %d subsumptions saved; average neg candidate size: %.2f' % (m, size_sum / m))


def get_restriction_values(r):
    if type(r.value) == ThingClass:
        return [str(r.value)]
    elif type(r.value) == And:
        v = str(r.value)
        return v.split(' & ')
    elif type(r.value) == Or:
        v = str(r.value)
        return v.split(' | ')


def get_test_neg_candidates_restriction(subcls, gt_restriction):
    restrictions_ancestors = onto.get_all_restriction_ancestors(cls=subcls)
    neg_restrictions = list()
    gt_values = get_restriction_values(r=gt_restriction)
    for r in onto.restrictions:
        values = get_restriction_values(r=r)
        if r not in restrictions_ancestors and r not in neg_restrictions:
            for v in values:
                if v in gt_values:
                    neg_restrictions.append(r)
                    break
    for r in onto.restrictions:
        if r not in restrictions_ancestors and r not in neg_restrictions:
            if r.property == gt_restriction.property:
                neg_restrictions.append(r)

    if len(neg_restrictions) > FLAGS.test_max_neg_size:
        neg_restrictions = random.sample(neg_restrictions, FLAGS.test_max_neg_size)

    random_restrictions = random.sample(onto.restrictions - {gt_restriction} - set(neg_restrictions) -
                                        restrictions_ancestors, FLAGS.test_neg_random_restriction_size)
    return neg_restrictions + random_restrictions


if FLAGS.restriction:
    with open(FLAGS.train_file, 'w') as f:
        for c1, c2 in train_subsumptions:
            f.write('%s,%s\n' % (c1.iri, str(c2)))
    with open(FLAGS.valid_file, 'w') as f:
        sizes = 0
        for c1, c2 in valid_subsumptions:
            c2_neg = get_test_neg_candidates_restriction(subcls=c1, gt_restriction=c2)
            sizes += len(c2_neg)
            strs = [str(r) for r in c2_neg]
            f.write('%s,%s,%s\n' % (c1.iri, str(c2), ','.join(strs)))
        print('valid candidate negative avg. size: %.1f' % (sizes / len(valid_subsumptions)))
    with open(FLAGS.test_file, 'w') as f:
        sizes = 0
        for c1, c2 in test_subsumptions:
            c2_neg = get_test_neg_candidates_restriction(subcls=c1, gt_restriction=c2)
            sizes += len(c2_neg)
            strs = [str(r) for r in c2_neg]
            f.write('%s,%s,%s\n' % (c1.iri, str(c2), ','.join(strs)))
        print('test candidate negative avg. size: %.1f' % (sizes / len(test_subsumptions)))

else:
    with open(FLAGS.train_file, 'w') as f:
        for c1, c2 in train_subsumptions:
            f.write('%s,%s\n' % (c1.iri, c2.iri))

    print('\n---- context candidates for validation subsumptions ----')
    context_candidate(output_file=FLAGS.valid_file, subsumptions=valid_subsumptions)

    print('\n---- context candidates for test subsumptions ----')
    context_candidate(output_file=FLAGS.test_file, subsumptions=test_subsumptions)
