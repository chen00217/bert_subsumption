import os
import random
import torch
from ontology import MyOntology
from bert.bert_train import BERTTrainer
from transformers import TrainingArguments


class Transfer:
    def __init__(self, config):
        self.config = config
        self.onto = MyOntology(onto_file=self.config['transfer']['transfer_ontology'],
                               label_property=config['transfer']['label_property'])
        self.samples = None

    def extract_samples(self):
        subsumptions = self.onto.get_declared_subsumption(restriction=False)
        size = self.config['transfer']['transfer_size'] if len(subsumptions) > self.config['transfer']['transfer_size'] \
            else len(subsumptions)
        positive_subsumptions = random.sample(subsumptions, size)

        classes = set(self.onto.get_named_classes())
        negative_subsumptions = list()
        for c1, _ in positive_subsumptions:
            neg_classes = random.sample(classes, 2)
            if neg_classes[0] not in c1.ancestors():
                negative_subsumptions.append([c1, neg_classes[0]])
            else:
                negative_subsumptions.append([c1, neg_classes[1]])

        positive_samples = self.onto.subsumptions_to_samples(subsumptions=positive_subsumptions, config=self.config,
                                                             sample_label=1)
        negative_samples = self.onto.subsumptions_to_samples(subsumptions=negative_subsumptions, config=self.config,
                                                             sample_label=0)
        print('Transfer train/valid: positive samples %d, negative samples %d' % (
        len(positive_samples), len(negative_samples)))
        samples = positive_samples + negative_samples
        random.shuffle(samples)
        self.samples = samples

    def train(self):
        tr_num = int(len(self.samples) * 0.9)
        tr, val, te = self.samples[0: tr_num], self.samples[tr_num:], list()

        torch.cuda.empty_cache()
        bert_trainer = BERTTrainer(self.config['transfer']['pretrained'], tr, val, te,
                                   max_length=self.config['task']['max_length'])
        if self.config['task']['context_type'] == 'directional':
            bert_trainer.add_special_tokens(['<SUB>', '<EOA>'])

        batch_size = self.config['task']["batch_size"]
        epoch_steps = len(bert_trainer.tra) // batch_size  # total steps of an epoch
        logging_steps = int(epoch_steps * 0.02) if int(epoch_steps * 0.02) > 0 else 5
        eval_steps = 5 * logging_steps
        training_args = TrainingArguments(
            output_dir=self.config['transfer']['output_dir'],
            num_train_epochs=self.config['transfer']['num_epochs'],
            per_device_train_batch_size=batch_size,
            per_device_eval_batch_size=batch_size,
            warmup_ratio=self.config['transfer']['warm_up_ratio'],
            weight_decay=0.01,
            logging_steps=logging_steps,
            logging_dir=f"{self.config['transfer']['output_dir']}/tb",
            eval_steps=eval_steps,
            evaluation_strategy="steps",
            do_train=True,
            do_eval=True,
            save_steps=eval_steps,
            load_best_model_at_end=True,
            save_total_limit=1,
            metric_for_best_model="accuracy",
            greater_is_better=True
        )
        bert_trainer.train(train_args=training_args, do_fine_tune=True)
        bert_trainer.trainer.save_model(
            output_dir=os.path.join(self.config['transfer']['output_dir'], self.config['transfer']['final_checkpoint']))
        print('Transfer model saved')
