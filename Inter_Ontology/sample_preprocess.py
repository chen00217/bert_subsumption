import sys
import argparse
import json
import random
from owlready2 import *
sys.path.append('../Intra_Ontology/')

from ontology import MyOntology

parser = argparse.ArgumentParser()
parser.add_argument('--config_file', type=str, default='./config_helis2foodon.json')
parser.add_argument('--test_max_neg_size', type=int, default=50)
parser.add_argument('--max_depth', type=int, default=3)
parser.add_argument('--max_width', type=int, default=8)
FLAGS, unparsed = parser.parse_known_args()
config = json.load(open(FLAGS.config_file))

src_onto = MyOntology(onto_file=config['task']['src_onto_file'], label_property=config['task']['src_label_property'])
tgt_onto = MyOntology(onto_file=config['task']['tgt_onto_file'], label_property=config['task']['tgt_label_property'])


def read_subsumption(mapping_file):
    subsumptions = list()
    with open(mapping_file) as f:
        for line in f.readlines()[1:]:
            tmp = line.strip().split('\t')[0:2]
            if tmp[0].split(':')[0] == 'omimps':
                subcls = 'http://www.omim.org/phenotypicSeries/' + tmp[0].split(':')[1]
            else:
                subcls = config['task']['src_prefix'] + tmp[0].split(':')[1]
            supcls = config['task']['tgt_prefix'] + tmp[1].split(':')[1]
            subsumptions.append([IRIS[subcls], IRIS[supcls]])
    return subsumptions


def get_test_neg_candidates(subclass, gt):
    all_nebs, seeds = set(), [gt]
    depth = 1
    while depth <= FLAGS.max_depth:
        new_seeds = set()
        for seed in seeds:
            nebs = tgt_onto.get_one_hop_neighbours(c=seed)
            new_seeds = new_seeds.union(nebs)
            all_nebs = all_nebs.union(nebs)
        depth += 1
        seeds = random.sample(new_seeds, FLAGS.max_width) if len(new_seeds) > FLAGS.max_width else new_seeds

    # all_nebs = all_nebs - subclass.ancestors() - {subclass}
    # The above is for negative candidates of intra-ontology subsumptions
    all_nebs = all_nebs - gt.ancestors() - {gt}

    if len(all_nebs) > FLAGS.test_max_neg_size:
        return random.sample(all_nebs, FLAGS.test_max_neg_size)
    else:
        return list(all_nebs)


def context_candidate(output_file, subsumptions):
    with open(output_file, 'w') as ff:
        size_sum = 0
        size_num = dict()
        m = 0
        for subcls, supcls in subsumptions:
            neg_candidates = get_test_neg_candidates(subclass=subcls, gt=supcls)
            size = len(neg_candidates)
            size_sum += size
            size_num[size] = size_num[size] + 1 if size in size_num else 1
            if size > 0:
                s = ','.join([c.iri for c in neg_candidates])
                ff.write('%s,%s,%s\n' % (subcls.iri, supcls.iri, s))
                m += 1
        print('\t The distribution of negative candidate size:')
        for size in range(FLAGS.test_max_neg_size + 1):
            if size in size_num:
                print('\t size: %d, num: %d' % (size, size_num[size]))
            else:
                print('\t size: %d, num: 0' % size)
        print('\t %d subsumptions saved; average neg candidate size: %.2f' % (m, size_sum / m))


if not config['task']['train_mapping_file'] == 'None':
    train_subsumptions = read_subsumption(mapping_file=config['task']['train_mapping_file'])
    with open(FLAGS.train_mapping_file, 'w') as f:
        for subsumption in train_subsumptions:
            f.write('%s,%s\n' % (subsumption[0].iri, subsumption[1].iri))

if not config['task']['valid_mapping_file'] == 'None' and not config['task']['test_mapping_file'] == 'None':
    valid_subsumptions = read_subsumption(mapping_file=config['task']['valid_mapping_file'])
    test_subsumptions = read_subsumption(mapping_file=config['task']['test_mapping_file'])
else:
    subsumptions = list()
    with open(config['task']['all_mapping_file']) as f:
        for line in f.readlines()[1:]:
            tmp = line.strip().split('\t')
            if 'foodon' in config['task']['tgt_onto_file']:
                if tmp[1] in tgt_onto.iri_label:
                    subsumptions.append([IRIS[tmp[0]], IRIS[tmp[1]]])
            else:
                subsumptions.append([IRIS[tmp[0]], IRIS[tmp[1]]])
    valid_size = int(0.25*len(subsumptions))
    random.shuffle(subsumptions)
    valid_subsumptions = subsumptions[0:valid_size]
    test_subsumptions = subsumptions[valid_size:]

print('\n---- context candidates for validation subsumptions ----')
context_candidate(output_file=config['task']['valid_subsumption_file'], subsumptions=valid_subsumptions)

print('\n---- context candidates for test subsumptions ----')
context_candidate(output_file=config['task']['test_subsumption_file'], subsumptions=test_subsumptions)
